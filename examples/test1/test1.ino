/*
 * Exercise Tom Rauschenbach's digital display board
 *
 * Pete Soper
 * March 27, 2017
 * Arena code stripped out, made simple demo February 1, 2020
 *
 * version 1.00
 */

#include "TRDISP.h"
#include <avr/pgmspace.h>

void setup() { 
  // Ser, Rclk, Sclk pin numbers
  TRDISP.begin(5,7,6);
} // setup

// Just continuously dislay a count as hex and decimal without and with
// leading zero blanking

void loop() {
  static uint32_t count = 0;
  TRDISP.display(count++, false, true); // zero suppress, display as hex
  delay(2000);
  TRDISP.display(count++, true, false); // no zero suppress, display as decimal
  delay(2000);
  if (count > 0xffff) {
    count = 0;
  }
}
