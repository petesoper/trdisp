/*
 * Tom Rauschenbach's Display library. Adapted from Tom's prototype code.
 *
 * Pete Soper
 * March 28, 2017
 * MIT license
 */

#include "TRDISP.h"
#include <avr/pgmspace.h>

const uint8_t digiTab[] PROGMEM = {
      0x3F, // 0
      0x06, // 1
      0x5B, // 2
      0x4F, // 3
      0x66, // 4
      0x6D, // 5
      0x7D, // 6
      0x07, // 7
      0x7F, // 8
      0x6F, // 9
      0x77, // a
      0x7C, // b
      0x39, // c
      0x5E, // d
      0x79, // e
      0x71, // f
      0x0,  // 10 blank
      0x1,  // 11 top
      0x2,  // 12 upper right
      0x4,  // 13 lower right
      0x8,  // 14 bottom
      0x10, // 15 lower left
      0x20, // 16 upper left
      0x40, // 17 middle
      0x80  // 18 decimal
    };

void TRDISP_CLASS::rOff() {
  cli();
  *rclk_p &= ~rclk_b;
  sei();
}

void TRDISP_CLASS::rOn() {
  cli();
  *rclk_p |= rclk_b;
  sei();
}

void TRDISP_CLASS::send(uint8_t val) {
  uint8_t mask = 0x80;
  cli();
  *rclk_p &= ~rclk_b;
  for (int i=0;i<8;i++) {
    *sclk_p &= ~sclk_b;
    *ser_p = val & mask ? *ser_p | ser_b : *ser_p & ~ser_b;
    *sclk_p |= sclk_b;
    mask >>= 1;
  }
  *rclk_p |= rclk_b;
  sei();
}

void TRDISP_CLASS::sendMap(uint8_t index) {
  send(pgm_read_byte_near(&digiTab[index]));
}

void TRDISP_CLASS::sendLED() {
  send(leds);
}

void TRDISP_CLASS::begin(uint8_t Ser, uint8_t Rclk, uint8_t Sclk) {
  pinMode(Ser, OUTPUT);
  pinMode(Rclk, OUTPUT);
  pinMode(Sclk, OUTPUT);
  rclk_p = portOutputRegister(digitalPinToPort(Rclk));
  rclk_b = digitalPinToBitMask(Rclk);
  sclk_p = portOutputRegister(digitalPinToPort(Sclk));
  sclk_b = digitalPinToBitMask(Sclk);
  ser_p = portOutputRegister(digitalPinToPort(Ser));
  ser_b = digitalPinToBitMask(Ser);
}

void TRDISP_CLASS::display(uint16_t value, bool zero_suppress, bool hex) {
    uint16_t div;
    uint8_t digits = 5;
    if (hex) {
      digits = 4;
      div = 4096;
    } else {
      div = 10000;
    }
    rOff();
    send(leds);
    bool seen=! zero_suppress;
    for (int i=1;i<=digits;i++) {
      uint8_t t = value / div;
      if (t == 0) {
	if (seen) {
	  t = pgm_read_byte_near(&digiTab[0]);
	} else {
	  t = 0;
        }
      } else {
	seen = true;
	t = pgm_read_byte_near(&digiTab[t]);
      }
	
      send(t);
      value %= div;
      if (hex) {
        div /= 16;
      } else {
        div /= 10;
      }
    }
    rOn();
}

void TRDISP_CLASS::setLED(uint8_t mask) {
  leds = mask;
}

TRDISP_CLASS TRDISP;
