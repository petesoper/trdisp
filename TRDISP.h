#ifndef H_TRDISP
#define H_TRDISP

/*
 * Tom Rauschenbach's Display library. Adapted from Tom's prototype code.
 *
 * Pete Soper
 * March 28, 2017
 * MIT license
 */


#include <Arduino.h>

class TRDISP_CLASS {
  public:
    void display(uint16_t value, bool zero_suppress = true, bool hex = false);
    void begin(uint8_t Ser = 7, uint8_t Rclk = 5, uint8_t Sclk = 6);
    void setLED(uint8_t value);
    void rOff();
    void rOn();
    void send(uint8_t val);
    void sendMap(uint8_t index);
    void sendLED();
    static const uint8_t GREEN = 0x80;
    static const uint8_t YELLOW = 0x40;
    static const uint8_t RED = 0x20;
    static const uint8_t BLUE = 0x10;
    static const uint8_t DASH = 0x17;
  private:
    uint8_t leds;
    volatile uint32_t *rclk_p;
    uint8_t rclk_b;
    volatile uint32_t *sclk_p;
    uint8_t sclk_b;
    volatile uint32_t *ser_p;
    uint8_t ser_b;
}; // class

extern TRDISP_CLASS TRDISP;


#endif // H_TRDISP
